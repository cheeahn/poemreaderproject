/*
FINAL UPDATE DATE
MAY 27TH 2018
*/

import java.net.HttpURLConnection;// required for HTML download
import java.net.URL;
import java.net.URLEncoder;
import java.io.InputStreamReader;// used to get our raw HTML source 
import guru.ttslib.*;
import ddf.minim.*;
import java.util.Arrays; // library for array to arraylist

ArrayList <String> lines; // store the lines of poems
PFont wordFont;
PFont urlFont;

Minim minim;
AudioPlayer audio; // for bgm

Speaker spk;
ProcessWebData web; // threads

boolean searchComplete; // indicates whether search is complete or not

void setup()
{
  size(1920,1080);
  //fullScreen();
  background(0);
 
 /*initialization*/
 lines = new ArrayList<String>();
 wordFont = loadFont("CourierNewPS-BoldMT-120.vlw");
 urlFont = loadFont("CourierNewPSMT-48.vlw");
 minim = new Minim(this);
 searchComplete = false;
 
 /*audio setup*/
 audio = minim.loadFile("untitled.mp3");
 audio.setGain(-18);
 audio.loop();
 
 /*load text file and save lines into array*/
 String[] l = loadStrings("selectedPoems.txt");
 for (String line : l)
 {
   lines.add(line);
 }
 
 /*create threads*/
 web = new ProcessWebData(new Speaker(lines));
}

/*for displaying purposes only*/
void draw()
{
  background(0);
  web.displayReadingLine();
  web.displaySearchURL();
  web.displayImageSearch();
}

/*deals with speaker*/
class Speaker extends Thread
{
  private ArrayList<String> readLines; // lines that speaker will read
  private String[] currWords;
  private String currWord; // current reading word
  private TTS speaker; // voice
  private boolean swtch; // check whether voice is reading next line
  private int currLineNumber;
  
  Speaker(ArrayList<String> l)
  {
    readLines = l;
    speaker = new TTS();
    currWord = null;
    currWords = null;
    swtch = false;
    currLineNumber = 0; // current line number initially 0
    start();
  }
  
  void displayLineRead()
  {
    if (currWord==null||currWord.equals("")) return;
    if (readLines==null||readLines.isEmpty()) return;
    textFont(wordFont);
    textSize(height/20);
    textAlign(CENTER);
    fill(0,255,0);
    text(readLines.get(currLineNumber), width/2, height/6);
  }
  
  String getCurrWord()
  {
    return currWord;
  }
  
  int getCurrLineLength()
  {
    if (currWords==null) return 0;
    return currWords.length;
  }
  
  boolean switchState()
  {
    return swtch;
  }
  
  void run()
  {
    while (true)
    {
      for (String l : readLines)
      {
         String processedLine = l.replaceAll("[^a-zA-Z0-9']", " ").toLowerCase().replaceAll("( )+", " ");
         currWords = processedLine.split(" ");
         for (String word : currWords)
         {
           //println("for loop");
           searchComplete = false;
           currWord = word;
           speaker.speak(word);
            try
            {
              Thread.sleep(300);
              synchronized(web)
              {
               // println("here");
                web.wait();
              }
            }
            catch (InterruptedException e){}
          }
          try
          {
            Thread.sleep(2000);
          }
          catch (InterruptedException e){}

          swtch = !swtch;
          currLineNumber = (currLineNumber+1)%readLines.size();
        }
      }
   }
}

/* deals with searching images and displaying image results*/
class ProcessWebData extends Thread
{
  private ArrayList<PImage> images;
  //private ArrayList<String> excludeFromSearch; // words to exclude from search
  
  private String prevSearchTerm; //previously searched term
  private String searchTerm; // term to search for
  
  private int offset;
  private String fileSize = "300mp";
  private String source = null; // string to save raw HTML source code
  private String[][] m;
  private Speaker spk; // speaker variable to get information about speaker
  
  private boolean switchState;
  
  ProcessWebData(Speaker spk)
  {
    this.spk = spk;
    images = new ArrayList<PImage>();
    prevSearchTerm = "";
    switchState = spk.switchState();
    start();
  }
  
  void displayReadingLine()
  {
    spk.displayLineRead();
  }
  
  void displaySearchURL()
  {
    if (m==null||m.length==0||images.size()==spk.getCurrLineLength()*5) return;
    textFont(urlFont);
    textSize(height/70);
    textAlign(CENTER);
    fill(0,255,0);
    try{
    for (int i = 0; i <m.length; i++)
      text(m[i][1], width/2, (height/20)*(5+i));
    }
    catch (Exception e) {
    }
  }
  
  void displayImageSearch()
  {
    if (images==null||images.isEmpty()) return;
    try
    {
      int counter = 0;
      for (PImage img : images)
      {
        img.resize(width/spk.getCurrLineLength(), height/spk.getCurrLineLength());
        imageMode(CENTER);
        image(img, (width/spk.getCurrLineLength())*((int)(counter/5)+0.5), height/2);
        counter++;
      }
    }
    catch (Exception e)
    {
      return;
    }
  }
  
  /* searching term - don't touch this part*/
  void search()
  {
    if (spk.getCurrWord()==null) return; // don't do anything if there is nothing is read
    
    searchTerm = spk.getCurrWord();
    println(searchTerm);
    
    offset = (int)(random(1,30));
    searchTerm = searchTerm.replaceAll(" ", "%20");
    try {
      URL query = new URL("http://images.google.com/images?gbv=1&start=" + offset + "&q=" + searchTerm + "&tbs=isz:lt,islt:" + fileSize);
      HttpURLConnection urlc = (HttpURLConnection) query.openConnection(); // start connection...
      urlc.setInstanceFollowRedirects(true);
      urlc.setRequestProperty("User-Agent", "");
      println("connecting");
      urlc.connect();
      println("connected");
      BufferedReader in = new BufferedReader(new InputStreamReader(urlc.getInputStream())); // stream in HTTP source to file
      StringBuffer response = new StringBuffer();
      char[] buffer = new char[1024];
      while (true) {
        int charsRead = in.read(buffer);
        if (charsRead == -1) {
          break;
        }
        response.append(buffer, 0, charsRead);
      }
      in.close(); // close input stream (also closes network connection)
      source = response.toString();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    // print full source code (for debugging)
    //println(source);
    
    if (source != null) {
      m = matchAll(source, "img height=\"\\d+\" src=\"([^\"]+)\"");
    }
    
    try
    {
      for (int i = 0; i < 5; i++)
      {
        images.add(loadImage(m[i][1],"png")); // save 5 images
      }
    }
    catch (NullPointerException e){}
    
    prevSearchTerm = searchTerm;
    searchComplete = true;
  }
  
  void run()
  {
    while (true)
    {
      synchronized(this)
      {
        if (switchState!=spk.switchState())
        {
          images.clear(); // clear only when moved to next line
          switchState = spk.switchState();
        }
        if (!prevSearchTerm.equals(spk.getCurrWord())&&spk.getCurrWord()!=null)
        {
          search();
        }
        //if (searchComplete)
        this.notify();
      }
      try
      {
        Thread.sleep(50);
      }
      catch (InterruptedException e){}
    }
  }
}
